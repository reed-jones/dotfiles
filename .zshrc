# install zgen if needed
if [ ! -d "${HOME}/.zgen" ]; then
    echo "Cloning zgen..."
    git clone https://github.com/tarjoilija/zgen.git "${HOME}/.zgen"
fi

# Load zgen script
source "${HOME}/.zgen/zgen.zsh"

# create zgen save if one doesn't exist
if ! zgen saved; then
    echo "Creating a zgen save"

    ##################
    # Load oh-my-zsh
    ##################

    zgen oh-my-zsh


    #####################    
    # oh-my-zsh plugins
    #####################

    zgen oh-my-zsh plugins/gitfast
    zgen oh-my-zsh plugins/git


    ################
    # Standalone plugins
    ################

    zgen loadall <<EOPLUGINS
        zsh-users/zsh-autosuggestions
        zdharma/fast-syntax-highlighting
        MichaelAquilina/zsh-you-should-use
        ael-code/zsh-colored-man-pages
        reed-jones/j0nz-zsh-common-aliases
EOPLUGINS


    #############################
    # Platform specific plugins
    #############################

    case "$(uname -s)" in
      Darwin) zgen load reed-jones/j0nz-zsh-macos-aliases ;;
      Linux) 	zgen load reed-jones/j0nz-zsh-linux-aliases ;;
    esac


    #########
    # Theme
    #########

    # this theme requires oh-my-zsh
    zgen load reed-jones/j0nz-zsh-theme j0nz-zsh

    ###########################
    # save all to init script
    ###########################

    zgen save

fi
